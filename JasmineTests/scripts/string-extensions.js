﻿String.prototype.capitalise = function () {
    return this.charAt(0).toUpperCase() + this.slice(1);
};
String.prototype.camelCaseToSpineCase = function () {
    return this.replace(/(?!^)(\B[A-Z])/g, ' $1').replace(/[_\s|\-]+(?=[a-zA-Z])/g, '-').toLowerCase();
};
String.prototype.spineCaseToCamelCase = function () {
    return this.replace(/^\s+|\s+$|\s+(?=\s)/g, "").replace(/(?!^)[\s|\-]+(\w)/g, function (m) { return m[1].capitalise(); });
};
String.prototype.format = function (param) {
    if (typeof param === "number" ) {
        return this.replace(/\{0\}/g, param);
    } else {
        return this.replace(/\{0\}/g, 'one').replace(/\{1\}/g, 'two').replace(/\{2\}/g, 'three');
    }
};