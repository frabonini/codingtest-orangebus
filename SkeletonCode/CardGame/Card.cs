﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkeletonCode.CardGame
{
    public class Card : ICard
    {
        private string suit;
        private int rank;
        public string[] suits = { "CLUBS", "HEARTS", "SPADES", "DIAMONDS" };
        
        public string Suit
        {
            get { return suit; }
        }

        public int Rank
        {
            get { return rank; }
        }

        public Card(string suit, int rank)
        {
            if (!suits.Contains(suit.ToUpper()))
            {
                throw new System.ArgumentException("Suit not valid");
            }
            else
            {
                this.suit = suit;
            }
            if (rank < 1 && rank > 13)
            {
                throw new System.ArgumentException("Rank not valid");
            }
            else
            {
                this.rank = rank;
            }
        }

    }
}
