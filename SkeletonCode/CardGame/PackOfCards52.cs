﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkeletonCode.CardGame
{
    class PackOfCards52 : IPackOfCards
    {
        public string[] suits = { "Clubs", "Hearts", "Spades", "Diamonds" };
        public enum Ranks { ace = 1, two, three, four, five, six, seven, eight, nine, ten, jack, queen, king };
        private List<Card> deck = new List<Card>();
        public PackOfCards52()
        {
            foreach(var suit in suits)
            {
                foreach(var rank in Enum.GetValues(typeof(Ranks)))
                {
                    deck.Add(new Card(suit, (int)rank));
                }
            }   
        }
        public List<Card> Deck {
            get{ return deck;}
        }
        public int Count
        {
            get
            {
                return Deck.Count;
            }
        }

        public IEnumerator<ICard> GetEnumerator()
        {
            return new PackEnum(deck);
        }

        public void Shuffle()
        {
            Random random = new Random();
            int i = Count;
            while (i > 1)
            {
                i--;
                int k = random.Next(i + 1);
                Card value = Deck[k];
                Deck[k] = Deck[i];
                Deck[i] = value;
            }
        }

        public ICard TakeCardFromTopOfPack()
        {
            Card first= Deck[0];
            Deck.Remove(first);
            return first;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    public class PackEnum : IEnumerator<ICard>
    {
        public List<Card> deck;
        int position = 0;

        public PackEnum(List<Card> list)
        {
            deck = list;
        }
        public Card Current
        {
            get
            {
                try
                {
                    return deck[position];
                }
                catch (IndexOutOfRangeException)
                {
                    throw new InvalidOperationException();
                }
            }
        }

        object IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }

        ICard IEnumerator<ICard>.Current
        {
            get
            {
                return Current;
            }
        }

        public void Dispose()
        { }

        public bool MoveNext()
        {
            position++;
            return (position < deck.Count);
        }

        public void Reset()
        {
            position = -1;
        }
    }
}
