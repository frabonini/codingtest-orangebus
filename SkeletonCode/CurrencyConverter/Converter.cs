﻿using System.Collections.Generic;

namespace SkeletonCode.CurrencyConverter
{
	public class Converter
	{
		public decimal Convert(string inputCurrency, string outputCurrency, decimal amount)
		{
            //create the dictionaries that contains valid currencies and their conversion factor
            Dictionary<string,decimal> conversion = new Dictionary<string, decimal>();

            //add the currencies and their corresponding factor
            conversion.Add("GBP", 0.8m);
            conversion.Add("USD", 1.25m);

            /*
             * check if the input and the output currencies are valid
             * if so, perform the conversion
             * if not, throw an exception
             */ 
            decimal inputFactor, outputFactor;
            if(conversion.TryGetValue(inputCurrency, out inputFactor) && conversion.TryGetValue(outputCurrency, out outputFactor))
            {
                amount = amount * outputFactor;
            } else
            {
                throw new System.Exception("The input or the output currency is not valid: it must be GBP or USD");
            }

            return amount;
		}
	}
}
