﻿using System;

namespace SkeletonCode.ReversingString
{
	public class StringUtilities
	{
		public string Reverse(string input)
		{
            /* FIRST REQUEST: make the test passes
             * 
             * check if the input string is null or empty and return an empty string
             * so now the test, that failed before, passes
             */
            if (string.IsNullOrEmpty(input))
                return string.Empty;

            /* SECOND REQUEST: improve implementation for large strings
             * 
             * new implementation of Reverse using Array.Reverse
             * which is a static method already optimized also
             * for usage with large strings
             */
            char[] output = input.ToCharArray();
            Array.Reverse(output);
            return new string(output);

            /* previous manual implementation
            string output = string.Empty;

			for(int i = input.Length - 1; i >= 0; i--)
			{
				output += input[i];
			}

			return output;*/
		}
	}
}
